package com.bunhann.retrofitsample1;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface DataService {

    @GET("province/custom")
    Call<List<Province>> getAllProvinces();

    @GET("province/custom/{id}")
    Call<Province> getProvince(@Path("id") int provinceId);

}
