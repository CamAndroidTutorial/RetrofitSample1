package com.bunhann.retrofitsample1;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Province {

    @SerializedName("PROCODE")
    @Expose
    private Integer proCode;


    @SerializedName("prefix")
    @Expose
    private String prefix;


    @SerializedName("PROVINCE")
    @Expose
    private String provinceName;


    @SerializedName("PROVINCE_KH")
    @Expose
    private String provinceNameKh;

    public Province() {
    }

    public Integer getProCode() {
        return proCode;
    }

    public void setProCode(Integer proCode) {
        this.proCode = proCode;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getProvinceNameKh() {
        return provinceNameKh;
    }

    public void setProvinceNameKh(String provinceNameKh) {
        this.provinceNameKh = provinceNameKh;
    }
}
